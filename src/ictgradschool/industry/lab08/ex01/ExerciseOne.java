package ictgradschool.industry.lab08.ex01;

import java.io.*;

public class ExerciseOne {

    public void start() {
        File myfile = new File("input2.txt");

        printNumEsWithBufferedReader(myfile);
        printNumEsWithFileReader(myfile);

    }

    private void printNumEsWithFileReader(File myfile)
    {
        int numE = 0;
        int total = 0;
        String tempStore="";
        try(FileReader a = new FileReader(myfile)){
            int character = 0;
            while ((character = a.read()) != -1)
            {
                tempStore += (char) character;
            }
            total = tempStore.length();

            for (int i = 0; i < total ; i++)
            {
                if (tempStore.charAt(i) == 'e' ||tempStore.charAt(i) == 'E' )
                {
                    numE++;
                }
            }
            System.out.println( "Filereader: There are "+total+" characters and "+numE+" e/E's");
        }
        catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader(File myfile) {

        String tempStore = "";
        int total = 0;
        int numE = 0;
        try(BufferedReader a = new BufferedReader(new FileReader(myfile))){
            String line = null;
            while ((line = a.readLine()) != null)
            {
                tempStore += line;
            }
            total = tempStore.length();
            for (int i = 0; i < total ; i++)
            {
                if (tempStore.charAt(i) == 'e' ||tempStore.charAt(i) == 'E' )
                {
                    numE++;
                }
            }
            System.out.println( "Bufferreader: There are "+total+" characters and "+numE+" e/E's");


        }
        catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
