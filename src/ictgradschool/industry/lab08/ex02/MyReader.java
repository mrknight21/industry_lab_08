package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MyReader {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a BufferedReader.

   while(true){
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();
        File reader = new File(fileName);
        if(reader.exists())
        {
            read(reader);
            break;
        }
        else
        {
            System.out.println("Sorry the file that you just typed in does not exist. Please check and try again.");
        }

    }}

    public void read (File reader)
    {
        try(  BufferedReader read = new BufferedReader(new FileReader(reader)))
        {
            String line;
            while ((line = read.readLine()) != null) {
                System.out.println(line);
            }
        }catch(IOException e) {
        System.out.println("Error: " + e.getMessage());
    }


    }


    public static void main(String[] args) {
        new MyReader().start();
    }
}
