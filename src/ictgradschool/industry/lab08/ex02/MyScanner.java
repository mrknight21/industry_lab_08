package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class MyScanner {

    public void start() {

        while(true){
            System.out.print("Enter a file name: ");
            String fileName = Keyboard.readInput();
            File reader = new File(fileName);
            if(reader.exists())
            {
                scanner(reader);
                break;
            }
            else
            {
                System.out.println("Sorry the file that you just typed in does not exist. Please check and try again.");
            }

        }



        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.
    }
    public void scanner (File scanner)
    {
        try (Scanner scan = new Scanner(scanner))
        {
            while (scan.hasNext())
            {

                String s = scan.nextLine();
                System.out.println(s);
            }

        }
        catch (IOException e)
        {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
